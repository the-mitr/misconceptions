<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1587639128892"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      Methods
    </p>
    <p style="text-align: center">
      to Probe
    </p>
    <p style="text-align: center">
      Misconceptions
    </p>
  </body>
</html>

</richcontent>
<hook NAME="MapStyle" zoom="3.0">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="9"/>
<node TEXT="Written Responses" POSITION="right" ID="ID_827418209" CREATED="1587639017205" MODIFIED="1587639025759">
<edge COLOR="#ff0000"/>
</node>
<node TEXT="Posters" POSITION="right" ID="ID_1327771247" CREATED="1587639026231" MODIFIED="1587639032102">
<edge COLOR="#0000ff"/>
</node>
<node TEXT="Card Sort" POSITION="right" ID="ID_243290457" CREATED="1587639035531" MODIFIED="1587639037617">
<edge COLOR="#00ff00"/>
</node>
<node TEXT="Thought Experiments" POSITION="right" ID="ID_552258305" CREATED="1587639037960" MODIFIED="1587639044146">
<edge COLOR="#ff00ff"/>
</node>
<node TEXT="Design and Make" POSITION="right" ID="ID_491825900" CREATED="1587639044492" MODIFIED="1587639048712">
<edge COLOR="#00ffff"/>
</node>
<node TEXT="Explain" POSITION="right" ID="ID_495678455" CREATED="1587639049000" MODIFIED="1587639052055">
<edge COLOR="#ffff00"/>
</node>
<node TEXT="Checklist/Questionnaire" POSITION="right" ID="ID_577218486" CREATED="1587639052315" MODIFIED="1587639062013">
<edge COLOR="#7c0000"/>
</node>
<node TEXT="Predict and Explain" POSITION="right" ID="ID_735217574" CREATED="1587639062530" MODIFIED="1587639070977">
<edge COLOR="#00007c"/>
</node>
<node TEXT="Practical Experiments" POSITION="right" ID="ID_1726508324" CREATED="1587639071371" MODIFIED="1587639080343">
<edge COLOR="#007c00"/>
</node>
</node>
</map>
