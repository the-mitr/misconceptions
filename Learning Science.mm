<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Learning&#xa;Science" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1587640305984"><hook NAME="MapStyle" zoom="3.0">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="8"/>
<node TEXT="Developing existing skills" POSITION="left" ID="ID_791312396" CREATED="1587641819644" MODIFIED="1587641826751">
<edge COLOR="#ffff00"/>
</node>
<node TEXT="Integrating existing skills" POSITION="left" ID="ID_1875186362" CREATED="1587641833324" MODIFIED="1587641844365">
<edge COLOR="#7c0000"/>
</node>
<node TEXT="Introducing new skills" POSITION="left" ID="ID_1479460961" CREATED="1587641846081" MODIFIED="1587641852944">
<edge COLOR="#00007c"/>
</node>
<node TEXT="Developing exisiting ideas" POSITION="right" ID="ID_1796019995" CREATED="1587640306940" MODIFIED="1587640318872">
<edge COLOR="#ff0000"/>
</node>
<node TEXT="Differentiating existing ideas" POSITION="right" ID="ID_407704664" CREATED="1587640319575" MODIFIED="1587640325972">
<edge COLOR="#0000ff"/>
</node>
<node TEXT="Integrating exisiting ideas" POSITION="right" ID="ID_524604657" CREATED="1587640326612" MODIFIED="1587640334289">
<edge COLOR="#00ff00"/>
</node>
<node TEXT="Changing exisiting ideas" POSITION="right" ID="ID_1841822922" CREATED="1587640334900" MODIFIED="1587640342370">
<edge COLOR="#ff00ff"/>
</node>
<node TEXT="Introducing new ideas" POSITION="right" ID="ID_1619426352" CREATED="1587640342902" MODIFIED="1587640346093">
<edge COLOR="#00ffff"/>
</node>
</node>
</map>
